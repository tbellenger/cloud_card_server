package au.com.bellsolutions.cardserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

import au.com.bellsolutions.android.emv.util.HexString;

/**
 * Servlet implementation class Server
 */
@WebServlet("/Server")
public class Server extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Card mCard;
	private CardChannel mChannel;

	/**
	 * @throws IOException
	 * @see HttpServlet#HttpServlet()
	 */
	public Server() throws IOException {
		super();
		initReader();
	}

	private void initReader() throws IOException {
		try {
			// show the list of available terminals
			TerminalFactory factory = TerminalFactory.getDefault();
			List<CardTerminal> terminals = factory.terminals().list();
			if (terminals.size() < 2) {
				throw new IOException("No contactless reader connected.");
			}
			// get the contactless terminal
			CardTerminal terminal = terminals.get(1);
			// establish a connection with the card
			mCard = terminal.connect("T=1");
			System.out.println("card: " + mCard);
			mChannel = mCard.getBasicChannel();
		} catch (CardException ce) {
			throw new IOException(ce.getMessage());
		}
	}
	
	private void cleanUp() {
		// disconnect
		try {
			System.out.println("Disconnecting reader");
			if (mCard != null) {
				mCard.disconnect(false);
			}
			mCard = null;
		} catch (CardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String apdu = request.getParameter("apdu");
		if (apdu == null || apdu.isEmpty()) {
			apdu = "00A4040000";
		}
		System.out.println("APDU-IN:" + apdu);
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		try {
			// Send apdu to card
			ResponseAPDU r = mChannel.transmit(new CommandAPDU(HexString.parseHexString(apdu)));
			// Output received apdu in JSON to client
			byte[] data = r.getBytes();
			System.out.println("APDU-OUT:" + HexString.hexify(data));
			out.println("{\'response\'=\'" + HexString.hexify(data, 0, data.length-2) + "\', \'sw\'=\'" + HexString.hexify(data, data.length - 2, 2) + "\'}");
		} catch (CardException e) {
			// try to recover
			e.printStackTrace();
			cleanUp();
			initReader();
			System.out.println("APDU-OUT: 6F00");
			out.println("{\'response\'=\'\', \'sw\'=\'6F00\', \'message\'=\'Try again\'}");
		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
			System.out.println("APDU-OUT: 6F00");
			out.println("{\'response\'=\'\', \'sw\'=\'6F00\'}");
		} catch (IllegalStateException ise) {
			// Card has been disconnected
			cleanUp();
			initReader();
			System.out.println("APDU-OUT: 6F00");
			out.println("{\'response\'=\'\', \'sw\'=\'6F00\', \'message\'=\'Try again\'}");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void finalize() {
		cleanUp();
	}

}
